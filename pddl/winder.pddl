;Header and description

(define (domain winder)

;remove requirements that are not needed
(:requirements :strips :fluents :durative-actions :timed-initial-literals :typing :conditional-effects :negative-preconditions :duration-inequalities :equality)

(:types
    winder stnest wrnest - zone
    stator - part
    observer - hooman
    wire - resource
    

;todo: enumerate types and their hierarchy here, e.g. car truck bus - vehicle
)

; un-comment following line if constants are needed
;(:constants )

(:predicates 
    (winder_available ?w - winder) ; станок занят печатью
    (winder_ready ?w - winder) ; станок готов
    (stator_placed ?w - winder  ?t - table) ; статор установлен
    (wire_ready ?w - winder ?wstrg - wire_storage ) ;готовность провода к печати
    (observer_free ?h - hooman) ;человек свободен
    (stator_ready ?p ?t) ; статор намотан


;todo: define predicates here
)


(:functions 


;todo: define numeric functions here
)

;define actions here


(:durative-action winder_check
    :parameters (?w - winder ?h - hooman)
    :duration (= ?duration 1)
    :condition (and 
        (at start (not(winder_ready ?w )))
        (at start (observer_free ?h ))
    )

    :effect (and 
        (at start (and 
            (not (observer_free ?h))  
        ))
        (at end (and
            (observer_free ?h)
            (winder_ready ?w) 
        ))
    )
)




(:durative-action winder_preparing
    :parameters (?w - winder ?h - hooman ?wstrg - wire_storage ?st - stator)
    :duration (= ?duration 1)
    :condition (and 
        (at start (and
            (winder_available ?w)
            (observer_free ?h)
            (or
                (not(wire_ready ?w ?wstrg ))
                (not(stator_placed ?w  ?t))
            )
            
            )
        ))

    
    :effect (and 
        (at start (and
            (not(observer_free ?h))
            (not(winder_available ?w))

        ))
        (at end (and
            (observer_free ?h)
            (winder_available ?w)
            (winder_ready ?w)
        ))
    )
)



(:durative-action start_task
    :parameters (?w - winder ?h - hooman ?wstrg - filament_storage )
    :duration (= ?duration 1)
    :condition (and 
        (at start (and
            (observer_free ?h)
            (winder_ready ?w)
            (winder_available ?w)
            (wire_ready ?w ?wstrg)
            ))

    )
    :effect (and 
        (at start (and
            (not (observer_free ?h))
            
        ))
        (at end (and
            (not(winder_available ?w))
            (observer_free ?h)            
        ))
    )

)




(:durative-action end_winding ; извлечение статора
    :parameters (?w - winder ?h - hooman )
    :duration (= ?duration print_eta ?t)
    :condition (and 
        (at start (and
            (observer_free ?h)
            (or
                (task_ready ?w ?t)
                (task_failed ?w ?t)
                       
            )
        ))

    )
    :effect (and 
        (at start (and
            (not(observer_free ?h))
            (not(winder_available ?w))

        ))
        (at end (and
            (observer_free ?h)
            (winder_available ?w)
            (winder_ready ?w)
            (stator_ready ?p  ?st)
        ))
    )
)




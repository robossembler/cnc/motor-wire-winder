;Header and description

(define (domain printer)

;remove requirements that are not needed
(:requirements :strips :fluents :durative-actions :timed-initial-literals :typing :conditional-effects :negative-preconditions :duration-inequalities :equality)

(:types
    printer table filament_storage - zone
    task - gcode
    observer - hooman
    filament - resource
    

;todo: enumerate types and their hierarchy here, e.g. car truck bus - vehicle
)

; un-comment following line if constants are needed
;(:constants )

(:predicates 
    (printer_available ?p - printer) ; принтер занят печатью
    (printer_ready ?p - printer) ; принтер готов
    (table_ready ?p - printer  ?t - table) ; готовность рабочего стола данного принтера
    (plastic_ready ?p - printer ?fstrg - filament_storage ) ;готовность пластика к печати
    (defined_task ?p - printer ) ;задание загружено в принтер
    (observer_free ?h - hooman) ;человек свободен
    (task_in ?t - task ?p - printer) ; задача на принтере
    (task_failed ?p ?t) ; печать провалена
    (task_ready ?p ?t) ; печать выполнена
; предикаты - принтер готов, пластик в наличии, пластика хватит на задание
; установить задание, посчитать пластик на него
; задание завершено
; рабочий стол принтера освобожден

;todo: define predicates here
)


(:functions 


;todo: define numeric functions here
)

;define actions here


(:durative-action printer_check
    :parameters (?p - printer ?h - hooman)
    :duration (= ?duration 1)
    :condition (and 
        (at start (not(printer_ready ?p )))
        (at start (observer_free ?h ))
    )

    :effect (and 
        (at start (and 
            (not (observer_free ?h))  
        ))
        (at end (and
            (observer_free ?h)
            (printer_ready ?p) 
        ))
    )
)




(:durative-action printer_preparing
    :parameters (?p - printer ?h - hooman ?fstrg - filament_storage ?t - table)
    :duration (= ?duration 1)
    :condition (and 
        (at start (and
            (printer_available ?p)
            (observer_free ?h)
            (or
                (not(plastic_ready ?p ?fstrg ))
                (not(table_ready ?p  ?t))
            )
            
            )
        ))

    
    :effect (and 
        (at start (and
            (not(observer_free ?h))
            (not(printer_available ?p))

        ))
        (at end (and
            (observer_free ?h)
            (printer_available ?p)
            (printer_ready ?p)
        ))
    )
)



(:durative-action start_task
    :parameters (?p - printer ?h - hooman ?fstrg - filament_storage ?t - gcode)
    :duration (= ?duration 1)
    :condition (and 
        (at start (and
            (observer_free ?h)
            (printer_ready ?p)
            (printer_available ?p)
            (plastic_ready ?p ?fstrg)
            ))

    )
    :effect (and 
        (at start (and
            (not (observer_free ?h))
            
        ))
        (at end (and
            (not(printer_available ?p))
            (task_in ?t ?p)
            (not(table_ready ?p  ?t))
            (observer_free ?h)
        ))
    )

)




(:durative-action end_printing
    :parameters (?p - printer ?h - hooman ?fstrg - filament_storage ?t - gcode)
    :duration (= ?duration print_eta ?t)
    :condition (and 
        (at start (and
            (observer_free ?h)
            (or
                (task_ready ?p ?t)
                (task_failed ?p ?t)
                       
            )
        ))

    )
    :effect (and 
        (at start (and
            (not(observer_free ?h))
            (not(printer_available ?p))

        ))
        (at end (and
            (observer_free ?h)
            (printer_available ?p)
            (printer_ready ?p)
            (table_ready ?p  ?t)
        ))
    )
)



